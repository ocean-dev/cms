# The CMS Service

This is the CMS Service for Ocean. The CMS service stores and caches localised
text and static assets such as images, videos, music files, etc.

## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up --build -d
```
Then, to run only the CMS service:
```
  docker-compose -f ../ocean/ocean.yml up --build cms_service
```
Then run the service setup/update rake task:
```
  docker-compose -f ../ocean/ocean.yml run --rm cms_service rake ocean:setup_all
```

The `ocean:setup_all` task executes the following rake tasks:
```
  db:create
  db:migrate
```
It can be run at any time.


## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up --build -d
```
Next create and migrate the test database:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e RAILS_ENV=test cms_service rake db:create
  docker-compose -f ../ocean/ocean.yml run --rm -e RAILS_ENV=test cms_service rake db:migrate
```
The source is accessible through a Docker volume, so any changes you make to the
source will be propagated to the container, but if you modify things like
gems, you need to rebuild the Docker image:
```
  docker-compose -f ../ocean/ocean.yml build cms_service
```
Then, to run the actual specs:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e LOG_LEVEL=fatal cms_service rspec
```
