# S3 resource interface
$s3 = Aws::S3::Resource.new(client: Aws::S3::Client.new(aws_config(
  endpoint: 'AWS_S3_ENDPOINT',
  force_path_style: ENV['AWS_CONFIG_LOCAL_MODE'].present?
)))

# CloudFront client
$cf = Aws::CloudFront::Client.new(aws_config)
