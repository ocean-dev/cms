# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180924173100) do

  create_table "media", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                                    null: false
    t.string   "app",          limit: 100
    t.string   "context",      limit: 100
    t.string   "locale",       limit: 10
    t.string   "tags"
    t.string   "content_type", limit: 100
    t.string   "url"
    t.string   "name",         limit: 100
    t.integer  "lock_version",             default: 0,  null: false
    t.string   "created_by"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "bytesize",                 default: 0,  null: false
    t.string   "updated_by"
    t.datetime "delete_at"
    t.string   "email"
    t.string   "file_name",                default: "", null: false
    t.string   "usage",                    default: "", null: false
    t.index ["app", "context", "name", "locale", "content_type"], name: "media_main_index", unique: true, using: :btree
    t.index ["app", "locale"], name: "index_media_on_app_and_locale", using: :btree
    t.index ["delete_at"], name: "index_media_on_delete_at", using: :btree
    t.index ["file_name"], name: "index_media_on_file_name", using: :btree
    t.index ["id"], name: "index_media_on_id", unique: true, using: :btree
  end

  create_table "texts", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "id",                                         null: false
    t.string   "app",          limit: 100
    t.string   "locale",       limit: 10
    t.string   "context",      limit: 100
    t.string   "name",         limit: 100
    t.string   "mime_type",    limit: 100
    t.string   "usage",        limit: 100,   default: ""
    t.text     "result",       limit: 65535
    t.integer  "lock_version",               default: 0,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "markdown",                   default: false, null: false
    t.text     "html",         limit: 65535
    t.string   "created_by"
    t.string   "updated_by"
    t.index ["app", "context", "locale", "name"], name: "main_index", unique: true, using: :btree
    t.index ["app", "context", "locale", "updated_at"], name: "index_texts_on_app_and_context_and_locale_and_updated_at", using: :btree
    t.index ["app", "locale", "updated_at"], name: "index_texts_on_app_and_locale_and_updated_at", using: :btree
    t.index ["created_at"], name: "index_texts_on_created_at", using: :btree
    t.index ["id"], name: "index_texts_on_id", unique: true, using: :btree
    t.index ["updated_at"], name: "index_texts_on_updated_at", using: :btree
  end

end
