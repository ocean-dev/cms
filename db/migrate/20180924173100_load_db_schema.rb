class LoadDbSchema < ActiveRecord::Migration[4.2]
  def up
    create_table "media", id: false, force: :cascade do |t|
      t.string   "id",           limit: 255, null: false
      t.string   "app",          limit: 100
      t.string   "context",      limit: 100
      t.string   "locale",       limit: 10
      t.string   "tags",         limit: 255
      t.string   "content_type", limit: 100
      t.string   "url",          limit: 255
      t.string   "name",         limit: 100
      t.integer  "lock_version", limit: 4,   default: 0,  null: false
      t.string   "created_by",   limit: 255
      t.datetime "created_at",                            null: false
      t.datetime "updated_at",                            null: false
      t.integer  "bytesize",     limit: 4,   default: 0,  null: false
      t.string   "updated_by",   limit: 255
      t.datetime "delete_at"
      t.string   "email",        limit: 255
      t.string   "file_name",    limit: 255, default: "", null: false
      t.string   "usage",        limit: 255, default: "", null: false
    end

    add_index "media", ["app", "context", "name", "locale", "content_type"], name: "media_main_index", unique: true, using: :btree
    add_index "media", ["app", "locale"], name: "index_media_on_app_and_locale", using: :btree
    add_index "media", ["delete_at"], name: "index_media_on_delete_at", using: :btree
    add_index "media", ["file_name"], name: "index_media_on_file_name", using: :btree
    add_index "media", ["id"], name: "index_media_on_id", unique: true, using: :btree

    create_table "texts", id: false, force: :cascade do |t|
      t.string   "id",           limit: 255, null: false
      t.string   "app",          limit: 100
      t.string   "locale",       limit: 10
      t.string   "context",      limit: 100
      t.string   "name",         limit: 100
      t.string   "mime_type",    limit: 100
      t.string   "usage",        limit: 100,   default: ""
      t.text     "result",       limit: 65535
      t.integer  "lock_version", limit: 4,     default: 0,     null: false
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "markdown",                   default: false, null: false
      t.text     "html",         limit: 65535
      t.string   "created_by",   limit: 255
      t.string   "updated_by",   limit: 255
    end

    add_index "texts", ["app", "context", "locale", "name"], name: "main_index", unique: true, using: :btree
    add_index "texts", ["app", "context", "locale", "updated_at"], name: "index_texts_on_app_and_context_and_locale_and_updated_at", using: :btree
    add_index "texts", ["app", "locale", "updated_at"], name: "index_texts_on_app_and_locale_and_updated_at", using: :btree
    add_index "texts", ["created_at"], name: "index_texts_on_created_at", using: :btree
    add_index "texts", ["id"], name: "index_texts_on_id", unique: true, using: :btree
    add_index "texts", ["updated_at"], name: "index_texts_on_updated_at", using: :btree
  end

  def down
    drop_table "media"
    drop_table "texts"
  end
end
