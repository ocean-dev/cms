require "spec_helper"

describe MediaController do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/media")).to route_to("media#index")
    end

    it "routes to #show" do
      expect(get("/v1/media/a-b-c-d-e")).to route_to("media#show", :id => "a-b-c-d-e")
    end

    it "routes to #create" do
      expect(post("/v1/media")).to route_to("media#create")
    end

    it "routes to #update" do
      expect(put("/v1/media/a-b-c-d-e")).to route_to("media#update", :id => "a-b-c-d-e")
    end

    it "routes to #destroy" do
      expect(delete("/v1/media/a-b-c-d-e")).to route_to("media#destroy", :id => "a-b-c-d-e")
    end

  end
end
