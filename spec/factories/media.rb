# == Schema Information
#
# Table name: media
#
#  id           :string           primary key
#  app          :string(100)
#  context      :string(100)
#  locale       :string(10)
#  tags         :string
#  content_type :string(100)
#  url          :string
#  name         :string(100)
#  lock_version :integer          default(0), not null
#  created_by   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  bytesize     :integer          default(0), not null
#  updated_by   :string
#  delete_at    :datetime
#  email        :string
#  file_name    :string           default(""), not null
#  usage        :string           default(""), not null
#
# Indexes
#
#  index_media_on_app_and_locale  (app,locale)
#  index_media_on_delete_at       (delete_at)
#  index_media_on_file_name       (file_name)
#  index_media_on_id              (id) UNIQUE
#  media_main_index               (app,context,name,locale,content_type) UNIQUE
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :medium do
    app          { "app" }
    context      { "context" }
    locale       { "sv-SE" }
    tags         { "foo,bar,baz" }
    content_type { "text/plain" }
    file_name    { "cartmans_mom.txt" }
    bytesize     { rand(100000) }
    name         { "name#{rand(10000)}" }
    lock_version { 0 }
    created_by   { "https://api.example.com/v1/api_users/a-b-c-d-e" }
    updated_by   { "https://api.example.com/v1/api_users/1-2-3-4-5" }
    delete_at    { 1.month.from_now }
    email        { "someone@example.com" }
    usage        { "" }
  end
end
