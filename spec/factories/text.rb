FactoryBot.define do
  factory :text do
    app       { "app_#{rand(10000)}" }
    context   { "context_#{rand(10000)}" }
    locale    { "sv-SE" }
    name      { "name_#{rand(10000)}" }
    mime_type { "text/plain" }
    lock_version { 0 }
    usage     { "" }
    result    { "Moxie" }
    created_by { "https://api.acme.com/v1/api_users/1-2-3-4-5" }
    updated_by { "https://api.acme.com/v1/api_users/1-2-3-4-5" }
  end
end