require 'spec_helper'

describe S3Storage do

  it "should have an .url method to compute the full Cloudfront URL" do
    expect(S3Storage.url(build :medium)).to eq "#{CLOUDFRONT_BASE_URL}/app/context/sv-SE/cartmans_mom.txt"
  end

  it "should have a .s3_key method to compute the full file name on S3" do
    expect(S3Storage.s3_key(build :medium)).to eq "app/context/sv-SE/cartmans_mom.txt"
  end
  
end
