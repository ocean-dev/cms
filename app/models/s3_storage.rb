class S3Storage

  def self.url(m)
    "#{CLOUDFRONT_BASE_URL}/#{s3_key(m)}"
  end

  def self.s3_key(m)
    "#{m.app}/#{m.context}/#{m.locale}/#{m.file_name}"
  end

  def invalidate_cloudfront_caching(m)
    $cf.create_invalidation({
      distribution_id: CLOUDFRONT_DISTRIBUTION_ID,
      invalidation_batch: {
        paths: {
          quantity: 1,
          items: [S3Storage.url(m) + "*"],
        },
        caller_reference: S3Storage.url(m)
      },
    })
  end


  #
  # These three methods manage a binary medium on S3.
  # When called from a Medium instance, the instance and/or the changes have
  # not yet been written to the DB.
  #
  def self.create_medium(m)
    bucket = $s3.bucket S3_BUCKET
    raise "S3 bucket #{S3_BUCKET} doesn't exist" unless bucket.exists?
    object = bucket.put_object(
      key: s3_key(m),
      body: Base64.decode64(m.payload),
      content_type: m.content_type
    )
    raise "S3 media couldn't be created" unless object.exists?
  end

  def self.update_medium(m)
    bucket = $s3.bucket S3_BUCKET
    raise "S3 bucket #{S3_BUCKET} doesn't exist" unless bucket.exists?
    object = bucket.put_object(
      key: s3_key(m),
      body: Base64.decode64(m.payload),
      content_type: m.content_type
    )
    raise "S3 media couldn't be updated" unless object.exists?
    invalidate_cloudfront_caching(m)
  end

  def self.delete_medium(m)
    bucket = $s3.bucket S3_BUCKET
    raise "S3 bucket #{S3_BUCKET} doesn't exist" unless bucket.exists?
    object = bucket.object(s3_key(m))
    return unless object.exists?
    object.delete
    invalidate_cloudfront_caching(m)
  end

end
