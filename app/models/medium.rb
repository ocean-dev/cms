# == Schema Information
#
# Table name: media
#
#  id           :string           primary key
#  app          :string(100)
#  context      :string(100)
#  locale       :string(10)
#  tags         :string
#  content_type :string(100)
#  url          :string
#  name         :string(100)
#  lock_version :integer          default(0), not null
#  created_by   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  bytesize     :integer          default(0), not null
#  updated_by   :string
#  delete_at    :datetime
#  email        :string
#  file_name    :string           default(""), not null
#  usage        :string           default(""), not null
#
# Indexes
#
#  index_media_on_app_and_locale  (app,locale)
#  index_media_on_delete_at       (delete_at)
#  index_media_on_file_name       (file_name)
#  index_media_on_id              (id) UNIQUE
#  media_main_index               (app,context,name,locale,content_type) UNIQUE
#

class Medium < ActiveRecord::Base

  self.primary_key = "id"

  def initialize(*)
    super
    self.id ||= SecureRandom.uuid
  end

  ocean_resource_model index: [:app, :context, :name, :locale, :content_type],
                       search: :file_name

  
  attr_accessible :app, :context, :name, :locale, :url, :content_type, 
                  :bytesize, :tags, :delete_at, :email, :file_name, 
                  :payload, :created_at, :updated_at, :created_by, 
                  :updated_by, :lock_version
                  
  attr_accessor :payload
                  
  validates :app,          :presence => true
  validates :context,      :presence => true
  validates :locale,       :presence => true, :format => /\A[a-z]{2}-[A-Z]{2}\z/
  validates :name,         :presence => true
  validates :content_type, :presence => true
  validates :lock_version, :presence => true
  validates :file_name,    :presence => true
  validates :url,          :presence => true
  
  before_validation :set_url
  
  def set_url
    self.url = S3Storage.url(self)
  end
    
  
  #
  # Transfer the base64 data to S3
  #
  before_create :create_on_s3
  before_update :update_on_s3
  before_destroy :delete_on_s3
  
  def create_on_s3
    return unless payload.present?
    S3Storage.create_medium(self)
  end
  
  def update_on_s3
    return true unless payload.present?
    S3Storage.update_medium(self)
  end
  
  def delete_on_s3
    S3Storage.delete_medium(self)
  end
    
end
